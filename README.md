Bitboys Timepicker
=====================

---
Extención de timepicker para horarios de Bitboy

---
Ejemplo de uso
==================
Dependencias incluidas en [/vendor]:

 * jquery.timepicker.css
 * jquery.timepicker.min.js
 * jquery-ui-1.10.4.custom.min.css
 * jquery-ui-1.10.4.custom.min.js


```html
<!-- Input para la fecha -->
<input type="text" id="bitboys_date">

<!-- Input para la hora -->
<input type="text" id="bitboys_time">
```