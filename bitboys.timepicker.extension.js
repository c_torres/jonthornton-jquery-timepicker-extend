
$(document).ready(function(){
	$('#bitboys_time').timepicker();

	$('#bitboys_date').datepicker({
		dateFormat: 'yy/mm/dd',
		minDate: 0,
		maxDate: "+15D",
	  	beforeShowDay: function(date) {
           var day = date.getDay();
           // disable sundays
           return [(day != 0), ''];
       	},
		onSelect: function(selectedDate) {
			setBitboysTimeRange();
        }
	});
});

function setBitboysTimeRange(){
	
	var currentDate = new Date();

	// set the hours, minutes, seconds and miliseconds of the currend date to 0
	currentDate.setHours(0);
	currentDate.setMinutes(0);
	currentDate.setSeconds(0);
	currentDate.setMilliseconds(0);

	// convert the selected date to a Date object
	var selectedDate = $('#bitboys_date').datepicker('getDate');
	

	// compare is the selected date y equals to the current date
	var is_today = false;
	if(selectedDate.valueOf() == currentDate.valueOf()){
		is_today = true;
	}

	// get the min time
	var minTime = getMinTime(selectedDate, is_today);

	var maxTime = getMaxTime(selectedDate.getDay());
	
	if(is_today){
		checkMinAndMaxTime(minTime, maxTime);
		return;
	}

	$('#bitboys_time').timepicker('option', {'minTime': minTime, 'maxTime': maxTime});

	$('#bitboys_time').val(minTime);
}

// ===============================================
// If the shippment is today and the min time is greater 
// than the maxTime, then add 1 day to the selected date
function checkMinAndMaxTime(minTime, maxTime){
	var minTime = minTime.split(':');
	var maxTime = maxTime.split(':');

	var minHour = parseInt(minTime[0]);
	var maxHour = parseInt(maxTime[0]);

	var minM = minTime[1].slice(2);
	var maxM = maxTime[1].slice(2);

	if(minM == 'pm') minHour += 12;
	if(maxM == 'pm' && maxHour > 12) maxHour += 12;

	if(minHour > maxHour){
		var date2 = $('#bitboys_date').datepicker('getDate');

        date2.setDate(date2.getDate()+1);

        if(date2.getDay() == 0)
        	date2.setDate(date2.getDate() + 1 );
        
     	$('#bitboys_date').datepicker('setDate', date2);
     	$('#bitboys_date').fadeOut();
     	$('#bitboys_date').fadeIn(2000);
     	setBitboysTimeRange();
	}
} 

// ============================================
// Calculate the min time available based on 
// the selected date 
// ============================================
function getMinTime(selectedDate, is_today){
	// if the selected date is today add hours to currend time
	if(is_today){
		var currentDate  = new Date();

		var new_date = currentDate.addHours(2);

		if(currentDate.getMinutes() >= 30){
			new_date = currentDate.addHours(3);
		}

		var hour = new_date.getHours();	

		var m = 'am';

		if(hour >= 12) m = 'pm'; 

		if(hour => 13) hour = hour - 12;
		if(hour == 12) hour = 1;

		return hour + ':00' + m;

	}else{
		// if the shippment is not today return the default hour
		if(selectedDate.getDay() == 6)
			return '9:30am'; // min time for saturday
		
		return '11:00am';// min time from monday to friday
	}
}

// ===========================================
// Get the max time base on the day of the week
// ============================================
function getMaxTime(dayOfTheWeek){
	if(dayOfTheWeek === 6){ 
		return '12:00pm'; 
	}
	return '7:00pm';
}

// =========================================
// Add hours to a Date object
// =========================================
Date.prototype.addHours= function(h){
    this.setHours(this.getHours()+h);
    return this;
}
